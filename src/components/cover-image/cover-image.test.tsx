import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { CoverImage } from '..';

it('CoverImage renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<CoverImage  imageUrl='https://pbs.twimg.com/profile_banners/10228272/1543592221/1500x500' />, div);
  ReactDOM.unmountComponentAtNode(div);
});